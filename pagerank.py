from pyspark import SparkContext
from pyspark import SparkConf
import sys

APP_NAME = 'page rank'
INPUT = sys.argv[1]
OUTPUT = sys.argv[2]
MASTER = sys.argv[3]
# argv[4] optional for running mode can be blank, part, or persist
ITERATIONS = 10

def page_rank(conf):
    # configuration
    conf = conf.setAppName(APP_NAME)
    sc = SparkContext(conf=conf)
    # core part of the script
    lines = sc.textFile(INPUT)
    # Create all pairs of (startNode, dstNode) from initial data.
    pairs = lines.map(lambda s: (s.split("\t")[0], s.split("\t")[1] if len(s.split("\t")) > 1 else ""))
    # Initialize the rank of all nodes to 1.
    ranks = pairs.reduceByKey(lambda x, y: 1.0)
    # Creates pairs in the form (startNode, {dstNode1, dstNode2, ,... dstNoden}).
    links = pairs.groupByKey().map(lambda s: (s[0], list(s[1])))

    for i in range(ITERATIONS):
        # Create all (dstNode, updateRank) pairs.
        contribs = links.join(ranks).flatMap(lambda url_links_rank: new_ranks(url_links_rank[1][0], url_links_rank[1][1]))
        # Sum up all updateRanks and apply for formula for all dstNodes.
        ranks = contribs.reduceByKey(lambda x, y: x + y).mapValues(lambda sum: .15 + .85 * sum)
        
    ranks.saveAsTextFile(OUTPUT)

def page_rank_part(conf):
    # configuration
    APP_NAME = 'page rank part'
    conf = conf.setAppName(APP_NAME)
    sc = SparkContext(conf=conf)
    # core part of the script
    lines = sc.textFile(INPUT)
    # Create all pairs of (startNode, dstNode) from initial data.
    pairs = lines.map(lambda s: (s.split("\t")[0], s.split("\t")[1] if len(s.split("\t")) > 1 else ""))
    # Initialize the rank of all nodes to 1.
    ranks = pairs.reduceByKey(lambda x, y: 1.0)
    # Creates pairs in the form (startNode, {dstNode1, dstNode2, ,... dstNoden}).
    links = pairs.groupByKey(numPartitions=700).map(lambda s: (s[0], list(s[1])))

    for i in range(ITERATIONS):
        # Create all (dstNode, updateRank) pairs.
        contribs = links.join(ranks).flatMap(lambda url_links_rank: new_ranks(url_links_rank[1][0], url_links_rank[1][1]), preservesPartitioning=True)
        # Sum up all updateRanks and apply for formula for all dstNodes.
        ranks = contribs.reduceByKey(lambda x, y: x + y).mapValues(lambda sum: .15 + .85 * sum)
        
    ranks.saveAsTextFile(OUTPUT)

def page_rank_persist(conf):
    # configuration
    APP_NAME = 'page rank persist'
    conf = conf.setAppName(APP_NAME)
    sc = SparkContext(conf=conf)
    # core part of the script
    lines = sc.textFile(INPUT)
    # Create all pairs of (startNode, dstNode) from initial data.
    pairs = lines.map(lambda s: (s.split("\t")[0], s.split("\t")[1] if len(s.split("\t")) > 1 else ""))
    # Initialize the rank of all nodes to 1.
    ranks = pairs.reduceByKey(lambda x, y: 1.0)
    # Creates pairs in the form (startNode, {dstNode1, dstNode2, ,... dstNoden}).
    links = pairs.groupByKey().map(lambda s: (s[0], s[1])).persist()

    for i in range(ITERATIONS):
        # Create all (dstNode, updateRank) pairs.
        contribs = links.join(ranks).flatMap(lambda url_links_rank: new_ranks(url_links_rank[1][0], url_links_rank[1][1]))
        # Sum up all updateRanks and apply for formula for all dstNodes.
        ranks = contribs.reduceByKey(lambda x, y: x + y).mapValues(lambda sum: .15 + .85 * sum)
        
    ranks.saveAsTextFile(OUTPUT)

# Function that elicits all tuples of nodes receiving rank values.
def new_ranks(links, rank):
    size_links = len(links)
    try:
        rank = float(rank)
    except ValueError:
        rank = 1.0
    for link in links:
        yield (link, rank/size_links)

if __name__ == '__main__':

    conf = SparkConf()
    conf = conf.setMaster(MASTER)
    conf = conf.set("spark.executor.memory", "5g")
    conf = conf.set("spark.executor.cores", "1")
    
    if len(sys.argv) > 4:
        if sys.argv[4] == "part":
            page_rank_part(conf)
        elif sys.argv[4] == "persist":
            page_rank_persist(conf)
        else:
            print("Please leave MODE argument blank, or enter 'persist' or 'part'.")
    else:
        page_rank(conf)