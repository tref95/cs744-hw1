# CS744-HW1

Part 1 ran locally with hellospark.py on a local spark cluster with 1 node (for memory purposes) using
https://github.com/mvillarrealb/docker-spark-cluster

Running Problem 1:

`/spark/bin/spark-submit /opt/spark-apps/hellospark.py`

`spark-2.2.0-bin-hadoop2.7/bin/spark-submit sort_iot.py hdfs://10.10.1.1:9000/export.csv hdfs://10.10.1.1:9000/iot_output.csv spark://c220g1-030613vm-1.wisc.cloudlab.us:7077`

a. Normal 55
b. persist 56
c. partition 700 1:30hr
d. break

19/09/25 19:47:47 INFO TaskSetManager: Task 512.0 in stage 6.0 (TID 4556) failed, but the task will not be re-executed (either because the task failed with a shuffle data fetch failure, so the previous stage needs to be re-run, or because a different copy of the task has already succeeded).
19/09/25 19:47:47 INFO TaskSchedulerImpl: Removed TaskSet 6.0, whose tasks have all completed, from pool
19/09/25 19:47:47 INFO DAGScheduler: Resubmitting ShuffleMapStage 5 (reduceByKey at /users/jdburge/pagerank.py:26) and ShuffleMapStage 6 (join at /users/jdburge/pagerank.py:24) due to fetch failure
19/09/25 19:47:47 INFO BlockManagerInfo: Added broadcast_8_piece0 in memory on 172.16.63.1:37883 (size: 5.8 KB, free: 2.5 GB)
19/09/25 19:47:47 INFO DAGScheduler: Resubmitting failed stages