# PAGERANK

run.sh takes four arguments, input file, output file, master node, and mode

input and output can be hdfs locations or on a drive accessible by all workers.
input can be a directory with INPUT/*
output is in list format, but will be partitioned in several files. e.g output.txt is actually output.txt/*.txt part files.

MODES:
* {blank} = normal execution
* part = part 2; partitioned execution
* persist = part 3; persistence

Program assumes only files in directory or regex path provided are data files. Files are assumed to be scrubbed of comments.

to run, `./run.sh args`

e.g.

normal: `./run.sh /proj/uwmadison744-f19-PG0/data-part3/enwiki-pages-articles/link-enwiki-20180601-pages-articles* hdfs://10.10.1.1:9000/out.txt spark://10.10.1.1:7077`
part 2: `./run.sh /proj/uwmadison744-f19-PG0/data-part3/enwiki-pages-articles/link-enwiki-20180601-pages-articles* hdfs://10.10.1.1:9000/out.txt spark://10.10.1.1:7077 part`
part 3: `./run.sh /proj/uwmadison744-f19-PG0/data-part3/enwiki-pages-articles/link-enwiki-20180601-pages-articles* hdfs://10.10.1.1:9000/iot-out.csv spark://10.10.1.1:7077 persist`
