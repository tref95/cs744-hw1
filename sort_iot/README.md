# SORT IOT

run.sh takes three arguments, input file, output file, and master node

input and output can be hdfs locations or on a drive accessible by all workers.
input can be a directory with INPUT/*
output is in csv format, but will be partitioned in several files. e.g output.csv is actually output.csv/*.csv part files.

Program assumes csv is clean with top row being a list of column headers and the rest of the data being lines of csv representations of data. Files are assumed to be scrubbed of comments.

to run, `./run.sh`

e.g. `./run.sh hdfs://10.10.1.1:9000/export.csv hdfs://10.10.1.1:9000/iot-out.csv spark://10.10.1.1:7077`